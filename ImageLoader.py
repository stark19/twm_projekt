import ntpath
import os
import glob
import cv2


class ImageLoader:
    def __init__(self, directory):
        self.directory = directory


    # Funkcja pomocnicza która odzyskuje nazwę pliku z podanej ścieżki
    def get_name(self, path):
        head, tail = ntpath.split(path)
        return tail or ntpath.basename(head)


    def load_images(self):
        data_path = os.path.join(self.directory,'*g')
        files = glob.glob(data_path)
        data = []
        labels = []

        for f1 in files:
            img = cv2.imread(f1)
            data.append(img)
            file_name = self.get_name(f1)
            file_name = file_name.split()
            file_labels = [os.path.splitext(name)[0] for name in file_name]
            labels.append(file_labels)
        return data, labels