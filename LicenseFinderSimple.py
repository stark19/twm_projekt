import numpy as np
import cv2
import pytesseract as pt
import imutils
from skimage.segmentation import clear_border


# Wymaga wczesniejszego zainstalowania Tesseract OCR
# Pyteseract jest jedynie interfejsem łączącym się z tesseractem

# Przekazanie ścieżki to programu tesseracta:
# Filmik który tłumaczy o co chodzi: https://youtu.be/QlhY5r5l6lg
pt.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

# Druga opcja to dodanie folderu z tesseractem do PATH, ale to wymaga zmian poza kodem
# Filmik który tłumaczy o co chodzi: https://youtu.be/2kWvk4C1pMo


class LicenseFinderSimple:
    def __init__(self, max_rat=5, min_rat=2.1, display_partial_results=True):
        self.max_rat = max_rat
        self.min_rat = min_rat
        self.display_partial_results = display_partial_results

    # Funkcja pomocnicza, która wyświetli w odpowiednich momentach poszczególne etapy działania programu
    def partial_result(self, title, image, wait=True):
        if self.display_partial_results:
            cv2.imshow(title, image)
        if wait:
            cv2.waitKey(0)
        

    def prepare_image(self, image, max_plates):
        gray_im = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        img_bfilter = cv2.bilateralFilter(gray_im, 11, 17, 17)
        img_edged = cv2.Canny(img_bfilter, 30, 200)

        self.partial_result("final_im", img_edged)
        return self.find_contours(img_edged, max_plates)


    # Funkcja znajdująca kontury poszczególnych obiektów na przekazanym obrazie
    # Wymaga ona dostarczenia wcześniej przygotowanego obrazu
    def find_contours(self, image, max_num):
        contours = cv2.findContours(image.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = imutils.grab_contours(contours)
        contours = sorted(contours, key=cv2.contourArea, reverse=True)[:max_num]
        return contours
        

    # Funkcja szukająca lokalizacji tablic na obrazie
    def extract_possible_plates(self, image, contours):
        found_plates_countours = []
        found_plates_roi = []
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        for contour in contours:
            x, y, w, h = cv2.boundingRect(contour)
            ratio = w/float(h)
            if ratio >= self.min_rat and ratio <= self.max_rat:
                found_plates_countours.append(contour)
                cut_out_plate = image[y:y+h, x:x + w]
                temp_roi = cv2.threshold(cut_out_plate, 0, 255, cv2.THRESH_BINARY_INV| cv2.THRESH_OTSU)[1]
                temp_roi = clear_border(temp_roi)
                found_plates_roi.append(temp_roi)
                self.partial_result("temp_roi", temp_roi)
                self.partial_result("cut_out_plate", cut_out_plate)
        return found_plates_countours, found_plates_roi


    def extract_plates_simple(self, image):
        gray_im = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        bfilter = cv2.bilateralFilter(gray_im, 11, 17, 17)
        edged = cv2.Canny(bfilter, 30, 200)
        countours = self.find_contours(edged,10)
        return self.extract_possible_plates(image, countours)


    def prepare_tesseract(self, page_seg_meth=7):
        allowed_characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        options = "-c tessedit_char_whitelist={}".format(allowed_characters)
        options += " --psm {}".format(page_seg_meth)
        return options


    def use_tesseract_ocr(self, image, max_contours=6, page_seg_meth=7):
        plate_texts = []
        plate_countours = []
        contours = self.prepare_image(image, max_contours)
        found_plates_countours, found_plates_roi = self.extract_possible_plates(image, contours)
        
        if not found_plates_roi or not found_plates_countours:
            found_plates_countours, found_plates_roi = self.extract_plates_simple(image)
        
        tess_options = self.prepare_tesseract(page_seg_meth)
        if found_plates_countours:
            for i in range(0, len(found_plates_roi)):
                temp_text = pt.image_to_string(found_plates_roi[i], config=tess_options)
                temp_text = "".join([charac if ord(charac) < 128 else "" for charac in temp_text]).strip()
                self.partial_result("temp_roi", found_plates_roi[i])
                plate_texts.append(temp_text)
                plate_countours.append(found_plates_countours[i])
        return plate_texts, plate_countours