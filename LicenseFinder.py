import numpy as np
import cv2
import pytesseract as pt
import imutils
from skimage.segmentation import clear_border


# Wymaga wczesniejszego zainstalowania Tesseract OCR
# Pyteseract jest jedynie interfejsem łączącym się z tesseractem

# Przekazanie ścieżki to programu tesseracta:
# Filmik który tłumaczy o co chodzi: https://youtu.be/QlhY5r5l6lg
pt.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

# Druga opcja to dodanie folderu z tesseractem do PATH, ale to wymaga zmian poza kodem
# Filmik który tłumaczy o co chodzi: https://youtu.be/2kWvk4C1pMo


class LicenseFinder:
    def __init__(self, max_rat=5, min_rat=2.1, display_partial_results=True):
        self.max_rat = max_rat
        self.min_rat = min_rat
        self.display_partial_results = display_partial_results


    # Funkcja pomocnicza, która wyświetli w odpowiednich momentach poszczególne etapy działania programu
    def partial_result(self, title, image, wait=True):
        if self.display_partial_results:
            cv2.imshow(title, image)
        if wait:
            cv2.waitKey(0)
        

    def prepare_image(self, image, max_plates):
        gray_im = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        self.partial_result("image", gray_im)

        blackh_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (13,5))
        blackhat_im = cv2.morphologyEx(gray_im, cv2.MORPH_BLACKHAT, blackh_kernel)
        self.partial_result("blackhat",blackhat_im)

        mask_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (10,10))
        mask = cv2.morphologyEx(gray_im, cv2.MORPH_CLOSE, mask_kernel)
        mask = cv2.threshold(mask, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        self.partial_result("mask", mask)

        sobel_im = self.get_sobel_im(blackhat_im)

        thresh_im = cv2.GaussianBlur(sobel_im, (5, 5), 0)
        self.partial_result("GaussianBlur", thresh_im)
        
        thresh_im = cv2.dilate(thresh_im, None, iterations=2)
        close_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        thresh_im = cv2.morphologyEx(thresh_im, cv2.MORPH_CLOSE, close_kernel)
        self.partial_result("MORPH_CLOSE", thresh_im)
        
        thresh_im = cv2.threshold(thresh_im, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        self.partial_result("threshold",thresh_im)

        thresh_im = cv2.erode(thresh_im, None, iterations=2)
        thresh_im = cv2.dilate(thresh_im, None, iterations=2)
        self.partial_result("thresh_im",thresh_im)

        final_im = cv2.bitwise_and(thresh_im, thresh_im, mask=mask)
        final_im = cv2.morphologyEx(final_im, cv2.MORPH_CLOSE, close_kernel)
        final_im = cv2.dilate(final_im, None, iterations=2)
        final_im = cv2.erode(final_im, None, iterations=2)
        self.partial_result("final_im", final_im)
        
        return self.find_contours(final_im, max_plates)


    # Funkcja wyekstrahowana z funkcji prepare_image ze względu na swoją dłguość
    def get_sobel_im(self, image):
        sobel_im = cv2.Sobel(image, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=-1)
        sobel_im = np.absolute(sobel_im)
        max_v = np.max(sobel_im)
        min_v = np.min(sobel_im)
        sobel_im = 255 * ((sobel_im - min_v) / (max_v - min_v))
        sobel_im = sobel_im.astype("uint8")
        self.partial_result("sobel",sobel_im)
        return sobel_im


    # Funkcja znajdująca kontury poszczególnych obiektów na przekazanym obrazie
    # Wymaga ona dostarczenia wcześniej przygotowanego obrazu
    def find_contours(self, image, max_num):
        contours = cv2.findContours(image.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = imutils.grab_contours(contours)
        contours = sorted(contours, key=cv2.contourArea, reverse=True)[:max_num]
        return contours
    

    # Funkcja szukająca lokalizacji tablic na obrazie
    def extract_possible_plates(self, image, contours):
        found_plates_countours = []
        found_plates_roi = []
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        for contour in contours:
            x, y, w, h = cv2.boundingRect(contour)
            ratio = w/float(h)
            if ratio >= self.min_rat and ratio <= self.max_rat:
                found_plates_countours.append(contour)
                cut_out_plate = image[y:y+h, x:x + w]
                temp_roi = cv2.threshold(cut_out_plate, 0, 255, cv2.THRESH_BINARY_INV| cv2.THRESH_OTSU)[1]
                temp_roi = clear_border(temp_roi)
                found_plates_roi.append(temp_roi)
                self.partial_result("temp_roi", temp_roi)
                self.partial_result("cut_out_plate", cut_out_plate)
        return found_plates_countours, found_plates_roi


    def extract_plates_simple(self, image):
        gray_im = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        bfilter = cv2.bilateralFilter(gray_im, 11, 17, 17)
        edged = cv2.Canny(bfilter, 30, 200)
        countours = self.find_contours(edged,10)
        return self.extract_possible_plates(image, countours)


    def prepare_tesseract(self, page_seg_meth=7):
        allowed_characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        options = "-c tessedit_char_whitelist={}".format(allowed_characters)
        options += " --psm {}".format(page_seg_meth)
        return options


    def use_tesseract_ocr(self, image, max_contours=6, page_seg_meth=7):
        plate_texts = []
        plate_countours = []
        contours = self.prepare_image(image, max_contours)
        found_plates_countours, found_plates_roi = self.extract_possible_plates(image, contours)
        
        if not found_plates_roi or not found_plates_countours:
            found_plates_countours, found_plates_roi = self.extract_plates_simple(image)
        
        tess_options = self.prepare_tesseract(page_seg_meth)    
        if found_plates_countours:
            for i in range(0, len(found_plates_roi)):
                temp_text = pt.image_to_string(found_plates_roi[i],config=tess_options)
                temp_text = "".join([charac if ord(charac) < 128 else "" for charac in temp_text]).strip()
                if temp_text:
                    self.partial_result("temp_roi", found_plates_roi[i])
                    plate_texts.append(temp_text)
                    plate_countours.append(found_plates_countours[i])
        return plate_texts, plate_countours