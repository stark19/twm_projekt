# Bartłomiej Gołębiewski Nr indeksu: 293371
# Marta Mejer Nr indeksu: 283377
# Techniki widzenia maszynowego

import cv2
import imutils
import os
import shutil

from ImageLoader import ImageLoader
from LicenseFinder import LicenseFinder


def find_licenses(folder_name):
  image_loader = ImageLoader(folder_name)
  test_images, ground_truth = image_loader.load_images()
  license_finder = LicenseFinder(display_partial_results=False)
  found_plates = []

  k=0
  for image in test_images:
    print(f"{k+1}/{len(test_images)}")
    image = imutils.resize(image, width=600)
    plate_texts, plate_countours = license_finder.use_tesseract_ocr(image)
    found_plates.append(plate_texts)

    # Oznaczenie wejsciowego obrazka ze znalezioną rejestracją
    if  plate_texts and plate_countours:
      for i in range(0, len(plate_texts)):
        box = cv2.boxPoints(cv2.minAreaRect(plate_countours[i]))
        box = box.astype("int")
        cv2.drawContours(image, [box], -1, (0,0,255),2)
        (x, y, w, h) = cv2.boundingRect(plate_countours[i])
        cv2.putText(image, plate_texts[i], (x, y - 15), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2)
      cv2.imwrite(f"{curdir}/{folder_name}_OUT/{''.join(ground_truth[k])}_labeled.png", image)
    k += 1

  save_results_to_file(found_plates, ground_truth)


def save_results_to_file(labels, ground_truth):
  points = 0
  with open("Output.txt", "w") as text_file:
    text_file.write("Prawidłowa Odczytana IleZnakówOdczytanychDobrze IleZnakówWykryto\n")
    real_count = 0
    found_count = 0
    valid_count = 0
    for i in range(len(labels)):
      real = ''.join(ground_truth[i])
      found = ''.join(labels[i])
      valid_chars = 0

      real_count += len(real)
      found_count += len(found)

      #zliczanie prawidłowo odczytanych znaków
      for k in found:
        if k in real:
          valid_chars += 1
      valid_count += valid_chars

      #sprawdzanie, czy rejestracje są identyczne
      if real == found:
        points += 1

      text_file.write(f"{real} {found} {round(valid_chars/len(real), 3)} {round(len(found)/len(real), 3)}\n")
    text_file.write(f"\n\nPoprawnie odczytano {points} na {len(ground_truth)} tablic.\n")
    text_file.write(f"Poprawnie odczytano {valid_count} na {real_count} znaków. \nWykryto znaków: {found_count} na {real_count}.\n")
  print(f"\nPoprawnie odczytano {points} na {len(ground_truth)} tablic.\n")
  print(f"Poprawnie odczytano {valid_count} na {real_count} znaków. \nWykryto znaków: {found_count} na {real_count}.\n")


def get_binary_accuracy(labels, groundTruth):
  points = 0
  for i in range(len(labels)):
    if ''.join(labels[i]) == ''.join(groundTruth[i]):
      points += 1
  return points/len(labels)


# Sprawdzenie ile liter zostało poprawnie odczytanych
def get_letters_diff(labels, groundTruth):
  pass
    

if __name__ == '__main__':
  folder_name = input("Podaj nazwę folderu: ")
  curdir = os.getcwd()
  dir_name = os.path.join(curdir, f"{folder_name}_OUT")
  if os.path.exists(dir_name):
    shutil.rmtree(dir_name)
  output_dir = os.mkdir(dir_name)

  find_licenses(folder_name)



